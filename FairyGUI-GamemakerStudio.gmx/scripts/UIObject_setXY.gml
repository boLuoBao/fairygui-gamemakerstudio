/// UIObject_setXY( GObject, x, y)
var this = argument0;
var xv = argument1 ;
var yv = argument2 ;
if this._x != xv or this._y != yv
{
    var dx = xv - this._x;
    var dy = yv - this._y;
    this._x = xv;
    this._y = yv;
    UIObject_handleXYChanged(this)   
}

