/// UIObject_setSize(GObject, wv, hv, ignorePivot = false)
var this = argument0;
var wv = argument1;
var hv = argument2;
var ignorePivot = argument3;

if this._rawWidth != wv or this._rawHeight != hv
{
    this._rawWidth = wv;
    this._rawHeight = hv;
    if (wv < this.minWidth)
        wv = this.minWidth;
    if (hv < this.minHeight)
        hv = this.minHeight;
    if (this.maxWidth > 0 and wv > this.maxWidth)
        wv = this.maxWidth;
    if (this.maxHeight > 0 and hv > this.maxHeight)
        hv = this.maxHeight;
    var dWidth = wv - this._width;
    var dHeight = hv - this._height;
    this._width = wv;
    this._height = hv;

    UIObject_handleSizeChanged(this)
    
    if this._pivotX != 0 or this._pivotY != 0
    {
        if !this._pivotAsAnchor
        {
            if (!ignorePivot)
                UIObject_setXY(this, this._x - this._pivotX * dWidth, this._y - this._pivotY * dHeight);
            else 
                UIObject_handleXYChanged(this)
        }
        else UIObject_handleXYChanged(this)
    }
    
    // todo 
}
