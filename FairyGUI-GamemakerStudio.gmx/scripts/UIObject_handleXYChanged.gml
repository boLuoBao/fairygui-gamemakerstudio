/// UIObject_handleXYChanged(GObject)
var this = argument0;
if !this._displayObject exit;

var xv = this._x;
var xy = this._y;

if (this._pivotAsAnchor) 
{
    xv -= this._pivotX * this._width;
    yv -= this._pivotY * this._height;
}
if (this._pixelSnapping) 
{
    xv = round(xv);
    yv = round(yv);
}

this._displayObject._x = xv + this._pivotOffsetX;
this._displayObject._y = yv + this._pivotOffsetY;
