/// UIPackage_LoadPackage(pkg)
var pkg = argument0;
var pkg_itemList = pkg[? 'itemList']
var pkg_itemById = pkg[? 'itemById']
var pkg_itemByName = pkg[? 'itemByName']

// 加载 sprites.bytes
var file = file_text_open_read("sprites.bytes")
file_text_readln(file)
var str;
var data = ds_list_create();
var sprites = pkg[? 'sprites'];
while !file_text_eof(file)
{
    str = file_text_readln(file)
    if string_length(str) == 0 
        continue;
    
    String_Parse(str, ' ', true, data)
    
    var sprite = UIPackage_AtlasSprite_New();
    var itemId = data[| 0];
    var binIndex = data[| 1];
    sprite[? "atlas"] = asset_get_index( "Main" + "_atlas" + binIndex);
    var rect;
    rect[0] = real(data[| 2]);  rect[1] = real(data[| 3]);
    rect[2] = real(data[| 4]);  rect[3] = real(data[| 5]);
    sprite[? "rect"] = rect;
    sprites[? itemId] = sprite;
}
ds_list_destroy(data)
file_text_close(file)

// 加载 Main.bytes 内的 package.xml
var descMap = pkg[? 'descPack'];
var packageXmlString = descMap[? "package.xml"];
if is_undefined(packageXmlString)
    show_error("FairyGUI: invalid package '" + 
        pkg[? 'name'] + 
        "', if the files is checked out from git, make sure to disable autocrlf option!",
        true)

var xmlNode = xf_read_xml( packageXmlString );
pkg[? "id"] = xf_get_attr(xmlNode, "id")
pkg[? "name"] = xf_get_attr(xmlNode, "name")

var resNode = xf_get_child(xmlNode, 0)
if xf_get_value(resNode) != 'resources'
    show_error("FairyGUI: invalid package xml",true)
    
var resList = xf_get_children( resNode )
var resListSize = ds_list_size(resList) 
var resItemNode;
var ItemSizeValueCache = ds_list_create();
for(var i = 0 ; i < resListSize; i++  )
{    
    resItemNode = resList[| i]
        
    var Pi = ds_map_create(); // packageItem
    Pi[? 'owner'] = pkg;
    Pi[? 'type'] = xf_get_value( resItemNode )
    Pi[? 'id'] = xf_get_attr(resItemNode, 'id')
    Pi[? 'name'] = xf_get_attr(resItemNode, 'name', '')
    
    var ItemExported = xf_get_attr(resItemNode, 'exported', 'false')
    if ItemExported == 'true' Pi[? 'exported'] = true
    
    var fileValue = xf_get_attr(resItemNode, 'file', undefined)
    if !is_undefined(fileValue)
        Pi[? 'file'] = fileValue
    
    var ItemSize = xf_get_attr(resItemNode, 'size', undefined)
    if !is_undefined(ItemSize)
    {
        String_Parse(ItemSize, ',', true, ItemSizeValueCache)
        Pi[? 'width'] = real(ItemSizeValueCache[| 0]);
        Pi[? 'height'] = real(ItemSizeValueCache[| 1]);
    }
    
    switch Pi[? 'type']
    {    
        case 'image':
            var attrScale = xf_get_attr(resItemNode, 'scale', '')
            if attrScale == '9grid'
            {
                var gridValue = xf_get_attr(resItemNode, 'scale9grid', '')
                if( gridValue != '' )
                {
                    String_Parse(gridValue, ',', true, ItemSizeValueCache)
                    var gridArray;
                    gridArray[0] = real(ItemSizeValueCache[| 0])
                    gridArray[1] = real(ItemSizeValueCache[| 1])
                    gridArray[2] = real(ItemSizeValueCache[| 2])
                    gridArray[3] = real(ItemSizeValueCache[| 3])
                    
                    Pi[? 'scale9Grid'] = gridArray
                    Pi[? 'tileGridIndice'] = real(xf_get_attr(resItemNode, 'gridTile', 0))
                }
            }
            else if attrScale == 'tile'
            {
                Pi[? 'scaleByTile'] = true
            }
            break;
        
        case 'component':
            /// todo: UIObjectFactory.ResolvePackageItemExtension(Pi);
            break;
    }
    
    ds_list_add(pkg_itemList, Pi)
    ds_map_add(pkg_itemById, Pi[? 'id'], Pi )
    ds_map_add(pkg_itemByName, Pi[? 'name'], Pi ) 
    //show_debug_message(json_encode(Pi))   
}
ds_list_destroy(ItemSizeValueCache)
xf_destroy(resList)
