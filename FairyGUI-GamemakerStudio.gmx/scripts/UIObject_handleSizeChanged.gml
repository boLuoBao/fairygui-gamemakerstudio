/// UIObject_handleSizeChanged(GObject)
var this = argument0;
if !this._displayObject exit;

if (this._sizeImplType == 0 || this.sourceWidth == 0 || this.sourceHeight == 0) 
{
    this._displayObject.scaleX = this._scaleX;
    this._displayObject.scaleY = this._scaleY;
}
else 
{
    this._displayObject.scaleX = this._width / this.sourceWidth * this._scaleX;
    this._displayObject.scaleY = this._height / this.sourceHeight * this._scaleY;
}

