///UIPackage_DecodeDesc(buffer, pkg)
// 解析描述文件
var descPack = ds_map_create();
var buf = argument0;
var pkg = argument1;
var pos = buffer_get_size(buf) - 22;
buffer_seek(buf, buffer_seek_start, pos+10);
 
var entryCount = buffer_read(buf, buffer_s16)
buffer_seek(buf , buffer_seek_start, pos+16)
pos = buffer_read(buf, buffer_s32)

// 获取入口

var len,len2;
for(var i = 0; i < entryCount; i++ )
{    
    buffer_seek(buf, buffer_seek_start, pos + 28)
    len = buffer_read(buf, buffer_u16)
    len2 = buffer_read(buf, buffer_u16) + buffer_read(buf, buffer_u16)    
    
    var strBuf = buffer_create(20, buffer_grow, 1)
    buffer_copy(buf, pos+46, len, strBuf, 0)
    var entryName = buffer_read(strBuf, buffer_string)
    buffer_delete(strBuf)
    
    var entryNameLastChar = string_char_at(entryName, string_length(entryName) -1 );
    if( entryNameLastChar != '/' and entryNameLastChar != '\\')
    {
        buffer_seek(buf, buffer_seek_start, pos+20)
        var size = buffer_read(buf, buffer_s32)
        buffer_seek(buf, buffer_seek_start, pos+42)
        var offset = buffer_read(buf, buffer_s32) + 30 + len;              
        
        var strBuf = buffer_create(20, buffer_grow, 1)
        buffer_copy(buf, offset, size, strBuf, 0)    
        buffer_seek(strBuf, buffer_seek_start,0)
        var desc = buffer_read(strBuf, buffer_text);        
        descPack[? entryName] = desc
        buffer_delete(strBuf)        
    }    
    pos += 46 + len + len2;
}

pkg[? 'descPack'] = descPack
