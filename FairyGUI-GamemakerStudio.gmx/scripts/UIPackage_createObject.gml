/// UIPackage_CreateObject(pkgName, resName)

var pkg = UIPackage_GetByName(argument0, argument1)
if is_undefined(pkg) return noone;

var itemByName = pkg[? 'itemByName'];
var Pi = itemByName[? argument1];
if is_undefined(Pi) return noone;

var g = UIObjectFactory_newObject(Pi);
return g;


