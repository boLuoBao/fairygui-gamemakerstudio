/// UIPackage_getItemAsset(pkg, Pi)
var pkg = argument0;
var Pi = argument1;
var type = Pi[? 'type']
switch type
{
    case 'component':
        var decoded = Pi[? 'decoded']
        if is_undefined(decoded) or ( !is_undefined(decoded) and !decoded )
        {
            Pi[? 'decoded'] = true
            var descPack = pkg[? 'descPack'];
            var str = descPack[? string(Pi[? 'id']) + '.xml' ];
            var xml = xf_read_xml(str);
            Pi[? 'componentData'] = xml;
            
            todo
        }
        return Pi[? 'componentData']
}
