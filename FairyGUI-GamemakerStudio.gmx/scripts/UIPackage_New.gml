/// UIPackage_New
// 实例化一个UIPackage

var pack = ds_map_create();
pack[? 'id'] = '';
pack[? 'name'] = '';
pack[? 'assetNamePrefix'] = '';
pack[? 'itemList'] = ds_list_create();
pack[? 'itemById'] = ds_map_create();
pack[? 'itemByName'] = ds_map_create();
pack[? 'descPack'] = undefined; //ds_map_create();
pack[? 'sprites'] = ds_map_create();

return pack;

