/// UIPackage_AddPackage(string descFileName)
if instance_number(UIPackage) == 0
    instance_create(0,0,UIPackage)

var descFileHandle = file_bin_open(argument0, 0)
var fileSize = file_bin_size(descFileHandle)
var descByte;
for(var i = 0 ; i < fileSize ; i ++)
    descByte[i] = file_bin_read_byte(descFileHandle)
file_bin_close(descFileHandle)

var pkg = UIPackage_New()
UIPackage_Create(pkg, descByte, fileSize)

if UIPackage_GetById(pkg[? 'id'])
    show_error("FairyGUI: Package id conflicts", true)

ds_list_add(UIPackage.packInstList , pkg)
UIPackage.packInstById[? pkg[? 'id']] = pkg
UIPackage.packInstByName[? pkg[? 'name']] = pkg

return pkg
