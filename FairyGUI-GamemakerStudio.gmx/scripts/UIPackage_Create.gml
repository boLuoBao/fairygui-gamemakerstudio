///UIPackage_Build(packageInst, descByte, descByteLength)
var pkg = argument0;
var descByte = argument1;
var descByteLength = argument2;

// bytes 存入 buffer
var temBuf = buffer_create(descByteLength, buffer_fast, 1)
for(var i = 0 ; i < descByteLength ; i++  )
    buffer_write( temBuf, buffer_u8, descByte[i])
var norBuf = buffer_create(descByteLength , buffer_fixed, 1)
buffer_copy(temBuf,0,descByteLength,norBuf,0)

UIPackage_DecodeDesc(norBuf, pkg)
UIPackage_LoadPackage(pkg)

buffer_delete(temBuf)
