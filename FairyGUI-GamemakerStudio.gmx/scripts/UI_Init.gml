/// UI_Init()
if instance_number(UIGlobal) == 0
    instance_create(0,0,UIGlobal)
    
    
enum ChildrenRenderOrder
	{
    		Ascent,
    		Descent,
    		Arch,
	}
